# A typed concatenative language

lambda-compose (lc) is a Go implementation of the concatenative programming
language designed by Robert Kleffner in his [dissertation][pdf].

[pdf]: https://www2.ccs.neu.edu/racket/pubs/dissertation-kleffner.pdf

Here's an example of what it can look like: a program that calculates ten
by doubling five.

	let dup =
	  bind x in (x x)
	in
	  (let double =
	     dup +
		 in
		   (5 double))



# Goals

The project aims to provide a practical experimention platform for typed
concatenative languages. The approach is provide an extensible machine
implementation, so that language features, such as syntax and constraint
systems, can be implemented externally.
