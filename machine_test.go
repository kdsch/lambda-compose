package lc

import "fmt"

func Example() {
	p := expression(let{
		variable: variable("dup"),
		defn:     expression(bind(variable("x"), variable("x"), variable("x"))),
		scope: expression(let{
			variable: variable("double"),
			defn:     expression(variable("dup"), plus{}),
			scope:    expression(num(5), variable("double")),
		}),
	})
	m := machine{expr: p}
	fmt.Println(m)
	m = m.run()
	if m.err != nil {
		fmt.Println(m.err)
		return
	}
	fmt.Println(m)
	// Output:
}
