package lc

import (
	"fmt"
)

/*
Syntax:

	expr = word->
	word = num | bool | var
	     | "plus" | "call" | "fix" | "if"
	     | "{" expr "}"
	     | "lambda" var "in" expr
	     | "let" var "=" expr "in" expr
	 num = 0, 1, 2, ...
	bool = true | false

Semantics:

	  stack = var->
	    var = num | bool | {expr}
	    num = 0, 1, 2, ...
	   bool = true | false
	machine = ‹stack | expr›

*/

type machine struct {
	stack *stack
	expr  expr
	err   error
}

func (m machine) literal(l word) machine {
	return machine{
		stack: m.stack.append(l),
		expr:  m.expr.rest(),
	}
}

func (m machine) reduce() (n machine) {
	defer catch(m, &n.err)
	return m.expr.first().reduce(m)
}

type reduceError struct {
	state      machine
	underlying error
}

func (r reduceError) Error() string {
	return fmt.Sprintf("%v: %v", r.state, r.underlying)
}

func catch(state machine, err *error) {
	if x, is := recover().(error); is {
		*err = reduceError{
			state:      state,
			underlying: x,
		}
	}
}

func (m machine) run() machine {
	for m.err == nil && !m.expr.empty() {
		m = m.reduce()
	}
	return m
}

type stack struct {
	value word
	next  *stack
}

func (s *stack) append(v word) *stack {
	return &stack{value: v, next: s}
}

type word interface {
	reduce(machine) machine
}

// literals
type num uint
type boole bool
type block struct {
	expr expr
}

// ‹s | n e› → ‹s n | e›
func (n num) reduce(m machine) machine {
	return m.literal(n)
}

// ‹s | b e› → ‹s b | e›
func (b boole) reduce(m machine) machine {
	return m.literal(b)
}

// ‹s | {e₁} e₂› → ‹s {e₁} | e₂›
func (b block) reduce(m machine) machine {
	return m.literal(b)
}

// functions
type plus struct{}
type call struct{}
type fix struct{}
type yf struct{}

// ‹s n₂ n₁ | + e› → ‹s (n₂ + n₁) | e›
func (plus) reduce(m machine) machine {
	right, rest := m.stack.value.(num), m.stack.next
	left, rest_ := rest.value.(num), rest.next
	return machine{
		stack: rest_.append(right + left),
		expr:  m.expr.rest(),
	}
}

// ‹s {e₁} | call e₂› → ‹s | e₁·e₂›
func (call) reduce(m machine) machine {
	blk, s := m.stack.value.(block), m.stack.next
	return machine{
		stack: s,
		expr:  blk.expr.concat(m.expr.rest()),
	}
}

// ‹s {e₁} | fix e₂› → ‹s {{e₁} fix} | e₁·e₂›
func (fix) reduce(m machine) machine {
	blk, s := m.stack.value.(block), m.stack.next
	return machine{
		stack: s.append(quote(blk, fix{})),
		expr:  blk.expr.concat(m.expr.rest()),
	}
}

func quote(words ...word) block {
	return block{
		expr: expr{words: words},
	}
}

// ‹s v₂ v₁ true  | if e› → ‹s v₁ | e›
// ‹s v₂ v₁ false | if e› → ‹s v₂ | e›
func (yf) reduce(m machine) machine {
	cond, rest := m.stack.value.(boole), m.stack.next
	right, rest_ := rest.value, rest.next
	left, rest__ := rest_.value, rest_.next
	calc := func() word {
		if cond {
			return right
		}
		return left
	}
	return machine{
		stack: &stack{value: calc(), next: rest__},
		expr:  m.expr.rest(),
	}
}

type variable string

func (v variable) reduce(m machine) machine {
	format := "unbound variable %q; state %v"
	m.err = fmt.Errorf(format, v, m)
	return m
}

// binders
type lambda struct {
	variable variable
	scope    expr
}

type let struct {
	variable    variable
	defn, scope expr
}

func bind(v variable, w ...word) lambda {
	return lambda{v, expression(w...)}
}

// ‹s v | (λx.e₁) e₂› → ‹s | ([v/x]e₁)·e₂›
// A value binding consumes a value from the
// top of the stack and substitutes it for all
// occurences of x in the enclosed expression e.
func (l lambda) reduce(m machine) machine {
	// for l.scope, substitute occurrences
	// of l.variable with m.stack.value
	value := expression(m.stack.value)
	scope := l.scope.subst(l.variable, value)
	return machine{
		stack: m.stack.next,
		expr:  scope.concat(m.expr.rest()),
	}
}

// ‹s | (let x = e₁ in e₂) e₃› → ‹s | ([e₁/x]e₂)·e₃›
func (l let) reduce(m machine) machine {
	scope := l.scope.subst(l.variable, l.defn)
	return machine{
		stack: m.stack,
		expr:  scope.concat(m.expr.rest()),
	}
}

type expr struct {
	words []word
}

func expression(words ...word) expr {
	return expr{words}
}

func (e expr) subst(x variable, defn expr) expr {
	var words []word
	for _, word := range e.words {
		switch v := word.(type) {
		case variable:
			if v == x {
				words = append(words, defn.words...)
			} else {
				words = append(words, v)
			}

		case block:
			words = append(words, block{
				expr: v.expr.subst(x, defn),
			})

		case lambda:
			words = append(words, lambda{
				variable: v.variable,
				scope:    v.scope.subst(x, defn),
			})

		case let:
			words = append(words, let{
				variable: v.variable,
				defn:     v.defn.subst(x, defn),
				scope:    v.scope.subst(x, defn),
			})

		default:
			words = append(words, v)
		}
	}
	return expr{words}
}

func (e expr) append(words ...word) expr {
	e.words = append(e.words, words...)
	return e
}

func (e expr) first() word {
	return e.words[0]
}

func (e expr) rest() expr {
	words := make([]word, e.max())
	copy(words, e.words[1:])
	return expr{words}
}

func (e expr) max() int {
	return len(e.words) - 1
}

func (e expr) empty() bool {
	return len(e.words) == 0
}

func (e expr) concat(f expr) expr {
	return e.append(f.words...)
}
