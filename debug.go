package lc

import (
	"fmt"
	"strings"
)

func (m machine) String() string {
	return fmt.Sprintf("{%v | %v}", m.stack, m.expr)
}

func (s *stack) String() string {
	var str []string
	for p := s; p != nil; p = p.next {
		str = append([]string{fmt.Sprintf("%v", p.value)}, str...)
	}
	return strings.Join(str, " ")
}


func (b block) String() string {
	return fmt.Sprintf("{%v}", b.expr)
}

func (plus) String() string { return "+" }
func (call) String() string { return "call" }
func (fix) String() string  { return "fix" }
func (yf) String() string   { return "if" }

func (l lambda) String() string {
	return fmt.Sprintf("lambda %v in (%v)", l.variable, l.scope)
}

func (l let) String() string {
	return fmt.Sprintf("let %v = %v in (%v)", l.variable, l.defn, l.scope)
}

func (e expr) String() string {
	var str []string
	for _, w := range e.words {
		str = append(str, fmt.Sprint(w))
	}
	return strings.Join(str, " ")
}

